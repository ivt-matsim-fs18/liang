package ch.ethz.matsim.students_fs18.liang.analysis;

import java.io.BufferedWriter;
import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.inject.Inject;
import org.apache.log4j.Logger;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.core.config.Config;
import org.matsim.core.config.groups.ControlerConfigGroup;
import org.matsim.core.config.groups.GlobalConfigGroup;
import org.matsim.core.controler.OutputDirectoryHierarchy;
import org.matsim.core.utils.charts.XYLineChart;
import org.matsim.core.utils.io.IOUtils;
import org.matsim.core.utils.io.UncheckedIOException;

public class DistanceDistributionStats {


    private final ControlerConfigGroup controlerConfigGroup;
    private final GlobalConfigGroup globalConfigGroup;
    private final BufferedWriter out;
    private final String fileName;
    private Map <String, List<Double>> samples;
    private Thread[] threads;
    private DistanceDistributionStats.StatsCalculator[] statsCalculators;
    private final AtomicBoolean hadException;
    private final DistanceDistributionStats.ExceptionHandler exceptionHandler;
    private static final Logger log = Logger.getLogger(DistanceDistributionStats.class);
    private Map <String, BinnedSampleCollector> binnedSamples;
    private final int numOfBins = 10;

    @Inject
    DistanceDistributionStats(ControlerConfigGroup controlerConfigGroup, GlobalConfigGroup globalConfigGroup, OutputDirectoryHierarchy controlerIO) {
        this(controlerConfigGroup, globalConfigGroup, controlerIO.getOutputFilename("distanceDistributionStats"), controlerConfigGroup.isCreateGraphs());
    }

    public DistanceDistributionStats(Config config, String filename, boolean createPNG) throws UncheckedIOException {
        this(config.controler(), config.global(), filename, createPNG);
    }

    DistanceDistributionStats(ControlerConfigGroup controlerConfigGroup, GlobalConfigGroup globalConfigGroup, String filename, boolean createPNG) {
        this.samples = new HashMap <>();
        this.binnedSamples = new HashMap<>();
        this.threads = null;
        this.statsCalculators = null;
        this.hadException = new AtomicBoolean(false);
        this.exceptionHandler = new DistanceDistributionStats.ExceptionHandler(this.hadException);
        this.controlerConfigGroup = controlerConfigGroup;
        this.globalConfigGroup = globalConfigGroup;
        this.fileName = filename;


        if (filename.toLowerCase(Locale.ROOT).endsWith(".txt")) {
            this.out = IOUtils.getBufferedWriter(filename);
        } else {
            this.out = IOUtils.getBufferedWriter(filename + ".txt");
        }

    }

    public void calculate(int iteration, Map<Id<Person>, Plan> map) {

        if (iteration < this.controlerConfigGroup.getLastIteration()){
            return;
        }

        int numOfThreads = this.globalConfigGroup.getNumberOfThreads();
        if (numOfThreads < 1) {
            numOfThreads = 1;
        }

        this.initThreads(numOfThreads);
        int roundRobin = 0;
        Iterator planIterator = map.values().iterator();

        while(planIterator.hasNext()) {
            Plan plan = (Plan)planIterator.next();
            this.statsCalculators[roundRobin++ % numOfThreads].addPerson(plan);
        }

        log.info("[" + this.getClass().getSimpleName() + "] using " + numOfThreads + " thread(s).");
        Thread[] var14 = this.threads;
        int var16 = var14.length;

        int nofLegTravelDistanceExecuted;
        Thread thread;
        for(nofLegTravelDistanceExecuted = 0; nofLegTravelDistanceExecuted < var16; ++nofLegTravelDistanceExecuted) {
            thread = var14[nofLegTravelDistanceExecuted];
            thread.start();
        }

        try {
            var14 = this.threads;
            var16 = var14.length;

            for(nofLegTravelDistanceExecuted = 0; nofLegTravelDistanceExecuted < var16; ++nofLegTravelDistanceExecuted) {
                thread = var14[nofLegTravelDistanceExecuted];
                thread.join();
            }
        } catch (InterruptedException var13) {
            throw new RuntimeException(var13);
        }

        log.info("[" + this.getClass().getSimpleName() + "] all threads finished.");
        if (this.hadException.get()) {
            throw new RuntimeException("Some threads crashed, thus not all persons may have been handled.");
        } else {
            nofLegTravelDistanceExecuted = 0;
            DistanceDistributionStats.StatsCalculator[] var17 = this.statsCalculators;
            int var9 = var17.length;

            for(int var10 = 0; var10 < var9; ++var10) {
                DistanceDistributionStats.StatsCalculator statsCalculator = var17[var10];

                nofLegTravelDistanceExecuted += statsCalculator.nofLegTravelDistanceExecuted;
            }

            this.statsCalculators = null;
            this.threads = null;

            if (this.samples != null) {

                IniBinnedSamples(samples);
                AbsoluteTooModeShare();

                XYLineChart chart = new XYLineChart("Leg Distance Distribution Statistics", "leg distance", "mode share");

                Iterator keyIterator = binnedSamples.keySet().iterator();

                while (keyIterator.hasNext()){
                    String key = (String) keyIterator.next();
                    double [] values = new double[numOfBins];
                    List<Double> valueList = binnedSamples.get(key).buildAbsoluteFrequencies();
                    for (int i = 0; i < valueList.size(); i++){
                        values[i] = valueList.get(i);
                    }

                    double [] right = new double[numOfBins];
                    List<Double> rightList = binnedSamples.get(key).getRight();
                    for (int i = 0; i < rightList.size(); i++){
                        right[i] = rightList.get(i);
                    }

                    chart.addSeries(key, right, values);
                }

                chart.addMatsimLogo();
                chart.saveAsPng(this.fileName + ".png", 800, 600);

            }
        }
    }

    public void close() {
        try {
            this.out.close();
        } catch (IOException var2) {
            var2.printStackTrace();
        }

    }

    private void initThreads(int numOfThreads) {
        if (this.threads != null) {
            throw new RuntimeException("threads are already initialized");
        } else {
            this.hadException.set(false);
            this.threads = new Thread[numOfThreads];
            this.statsCalculators = new DistanceDistributionStats.StatsCalculator[numOfThreads];

            for(int i = 0; i < numOfThreads; ++i) {
                DistanceDistributionStats.StatsCalculator statsCalculatorThread = new DistanceDistributionStats.StatsCalculator();
                Thread thread = new Thread(statsCalculatorThread, this.getClass().getSimpleName() + "." + DistanceDistributionStats.StatsCalculator.class.getSimpleName() + "." + i);
                thread.setUncaughtExceptionHandler(this.exceptionHandler);
                this.threads[i] = thread;
                this.statsCalculators[i] = statsCalculatorThread;
            }
        }
    }

    private static class ExceptionHandler implements UncaughtExceptionHandler {
        private final AtomicBoolean hadException;

        public ExceptionHandler(AtomicBoolean hadException) {
            this.hadException = hadException;
        }

        public void uncaughtException(Thread t, Throwable e) {
            DistanceDistributionStats.log.error("Thread " + t.getName() + " died with exception. Will stop after all threads finished.", e);
            this.hadException.set(true);
        }
    }

    private class StatsCalculator implements Runnable {
        int nofLegTravelDistanceExecuted;
        private Collection<Plan> persons;

        private StatsCalculator() {
            this.nofLegTravelDistanceExecuted = 0;
            this.persons = new ArrayList();
        }

        public void addPerson(Plan plan) {
            this.persons.add(plan);
        }

        public void run() {
            for(Iterator var1 = this.persons.iterator(); var1.hasNext(); ++this.nofLegTravelDistanceExecuted) {
                Plan plan = (Plan)var1.next();
                getLegTravelDistance(plan);
            }
        }

        private void getLegTravelDistance(Plan plan) {
            Iterator var5 = plan.getPlanElements().iterator();

            while(var5.hasNext()) {
                PlanElement pe = (PlanElement)var5.next();
                if (pe instanceof Leg) {
                    Leg leg = (Leg)pe;
                    double distance = leg.getRoute().getDistance();
                    String mode = leg.getMode();
                    if (!Double.isNaN(distance)) {
                        if (!samples.containsKey(mode)){
                            List <Double> distanceList = Collections.synchronizedList(new ArrayList<> (Arrays.asList(distance)));
                            samples.put(mode, distanceList);
                        }
                        else {
                            samples.get(mode).add(distance);
                        }
                    }
                }
            }
        }
    }

    private void IniBinnedSamples(Map <String, List<Double>> samples){

        double maxDistance = 0;


        Iterator keyIterator1 = samples.keySet().iterator();
        while (keyIterator1.hasNext()){
            String key = (String)keyIterator1.next();
            samples.get(key).removeAll(Collections.singleton(null));
            List<Double> distanceList = samples.get(key);


            if (Collections.max(distanceList) >= maxDistance){
                maxDistance = Collections.max(distanceList);
            }
        }

        List <Double> right = new ArrayList<>();

        for (int i = 1; i <= numOfBins; i++) {
            right.add(maxDistance / numOfBins * i);
        }

        Iterator keyIterator2 = samples.keySet().iterator();
        while (keyIterator2.hasNext()){
            String key = (String) keyIterator2.next();

            binnedSamples.put(key, new BinnedSampleCollector(right));

            Iterator modeIterator = samples.get(key).iterator();
            while (modeIterator.hasNext()){
                double distance = (double) modeIterator.next();
                binnedSamples.get(key).addSampleToBin(distance);
            }
        }
    }

    private void AbsoluteTooModeShare(){
        double [] sumList = new double [numOfBins];
        List<Double> rightList = binnedSamples.get("car").getRight(); // need improved

        DecimalFormat df = new DecimalFormat("#.##");

        try {
            this.out.write("mode share\t");
            for (Iterator iterator = rightList.iterator(); iterator.hasNext();){
                this.out.write(df.format(iterator.next()) + "\t");
            }
            this.out.write("\n");
        } catch (IOException var12) {
            var12.printStackTrace();
        }

        Iterator keyIterator1 = this.binnedSamples.keySet().iterator();
        while (keyIterator1.hasNext()){
            String key = (String)keyIterator1.next();
            List<Double> binList = this.binnedSamples.get(key).buildAbsoluteFrequencies();
            for (int i = 0; i < binList.size(); i++){
                sumList[i] += binList.get(i);
            }
        }

        Iterator keyIterator2 = this.binnedSamples.keySet().iterator();
        while (keyIterator2.hasNext()){
            String key = (String)keyIterator2.next();
            List<Double> binList = this.binnedSamples.get(key).buildAbsoluteFrequencies();
            for (int i = 0; i < binList.size(); i++){
                double frequency = binList.get(i);
                if (sumList[i] > 0){
                    binList.set(i, frequency / sumList[i]);
                }
                else
                    binList.set(i, 0.0);
            }

            try {
                this.out.write(key + "\t");
                for (Iterator iterator = binList.iterator(); iterator.hasNext();){
                    this.out.write(df.format(iterator.next()) + "\t");
                }
                this.out.write("\n");
            } catch (IOException var12) {
                var12.printStackTrace();
            }
        }
    }
}
