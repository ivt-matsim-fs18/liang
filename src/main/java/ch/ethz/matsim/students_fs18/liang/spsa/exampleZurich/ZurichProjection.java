package ch.ethz.matsim.students_fs18.liang.spsa.exampleZurich;

import ch.ethz.matsim.students_fs18.liang.spsa.exampleZurich.SPSAParameters;
import ch.ethz.matsim.students_fs18.liang.spsa.projection.SPSAProjection;

import java.util.ArrayList;
import java.util.List;

public class ZurichProjection implements SPSAProjection {
    final private SPSAParameters parameters;

    public ZurichProjection(SPSAParameters parameters) {
        this.parameters = parameters;
    }

    @Override
    public List<Double> projectGradientCandidate(double c, List<Double> perturbation, List<Double> candidate) {

        List<Double> newCandidate = new ArrayList<>();


        for (int i = 0; i < 4; i++){
            double criticalConstant = candidate.get(i * 2) - Math.abs(c * perturbation.get(i * 2));
            double constantOffset = 0.0;

            if (criticalConstant> parameters.maximumConstant) {
                constantOffset = criticalConstant - parameters.maximumConstant;
            }

            newCandidate.add(candidate.get(i * 2) + constantOffset);

            double criticalUtil = candidate.get(i * 2 + 1) - Math.abs(c * perturbation.get(i * 2 + 1));
            double utilOffset = 0.0;

            if (criticalUtil > parameters.maximumUtilTravel) {
                utilOffset = criticalUtil - parameters.maximumUtilTravel;
            }

            newCandidate.add(candidate.get(i * 2 + 1) + utilOffset);
        }

        /*
        double critialUtilMoney = candidate.get(8) - Math.abs(c * perturbation.get(8));
        double utilMoneyOffset = 0.0;

        if (critialUtilMoney < parameters.minimumUtilMoney) {
            utilMoneyOffset = parameters.minimumUtilMoney - critialUtilMoney;
        }

        newCandidate.add(candidate.get(8) + utilMoneyOffset);

        double critialPerforming = candidate.get(9) - Math.abs(c * perturbation.get(9));
        double performingOffset = 0.0;

        if (critialPerforming < parameters.minimumPerforming) {
            performingOffset = parameters.minimumPerforming - critialPerforming;
        }

        newCandidate.add(candidate.get(9) + performingOffset);
         */

        return newCandidate;
    }

    @Override
    public List<Double> projectObjectiveCandidate(List<Double> candidate) {
        List<Double> newCandidate = new ArrayList<>();

        for (int i = 0; i < candidate.size(); i++){
            if (i < 8){
                if (i % 2 == 0){
                    newCandidate.add(Math.min(parameters.maximumConstant, candidate.get(i)));
                }
                else{
                    newCandidate.add(Math.min(parameters.maximumUtilTravel, candidate.get(i)));
                }
            }
            else if (i == 8){
                newCandidate.add(Math.max(parameters.minimumUtilMoney, candidate.get(i)));
            }
            else if ( i == 9){
                newCandidate.add(Math.max(parameters.minimumPerforming, candidate.get(i)));
            }

        }
        return newCandidate;
    }
}
