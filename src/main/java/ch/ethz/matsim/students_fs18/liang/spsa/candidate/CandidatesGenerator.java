package ch.ethz.matsim.students_fs18.liang.spsa.candidate;

import ch.ethz.matsim.students_fs18.liang.spsa.SPSAEnvironment;
import ch.ethz.matsim.students_fs18.liang.spsa.SPSAIteration;
import ch.ethz.matsim.students_fs18.liang.spsa.SPSASequence;
import ch.ethz.matsim.students_fs18.liang.spsa.SPSAState;
import ch.ethz.matsim.students_fs18.liang.spsa.projection.SPSAProjection;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.List;

public interface CandidatesGenerator {

    void update (SPSAEnvironment spsaEnvironment);

    List<Double> generate(SPSAIteration iteration) throws IOException;

}

