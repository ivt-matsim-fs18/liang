package ch.ethz.matsim.students_fs18.liang.analysis;

import org.matsim.core.controler.AbstractModule;


public class DistanceDistributionModule extends AbstractModule {
    public DistanceDistributionModule() {

    }

    public void install() {
        this.bind(DistanceDistributionStats.class).asEagerSingleton();
        this.addControlerListenerBinding().to(DistanceDistributionStatsControlerListener.class);
    }

}
