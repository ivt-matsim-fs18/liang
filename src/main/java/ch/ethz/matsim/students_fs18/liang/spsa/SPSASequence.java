package ch.ethz.matsim.students_fs18.liang.spsa;

public interface SPSASequence {
	double getGradientFactor(int n);
	double getPerturbationFactor(int n);
}
