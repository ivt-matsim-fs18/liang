package ch.ethz.matsim.students_fs18.liang.spsa.exampleZurich;

import ch.ethz.matsim.students_fs18.liang.framework.simulation.SimulationDescription;
import ch.ethz.matsim.students_fs18.liang.spsa.SPSADescriptionFactory;

import java.util.List;

public class ZurichDescriptionFactory implements SPSADescriptionFactory {
    @Override
    public SimulationDescription create(List<Double> candidate) {
        ZurichSimulationDescription description = new ZurichSimulationDescription();
        description.carConstant = candidate.get(0);
        description.carUtil = candidate.get(1);
        description.ptConstant = candidate.get(2);
        description.ptUtil = candidate.get(3);
        description.bikeConstant = candidate.get(4);
        description.bikeUtil = candidate.get(5);
        description.walkConstant = candidate.get(6);
        description.walkUtil = candidate.get(7);
        //description.utilOfMoney = candidate.get(8);
        //description.performingPara = candidate.get(9);

        return description;
    }
}
