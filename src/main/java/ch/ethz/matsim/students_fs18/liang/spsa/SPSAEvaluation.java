package ch.ethz.matsim.students_fs18.liang.spsa;

import ch.ethz.matsim.students_fs18.liang.framework.simulation.SimulationDescription;

import java.util.LinkedList;
import java.util.List;

public class SPSAEvaluation {
    public String simulationId = null;
    public SimulationDescription description = null;
    public Double objective = null;

    public List<Double> intermediateObjectives = new LinkedList<>();
    public int lastIntermediateObjectiveIteration = -1;
}
