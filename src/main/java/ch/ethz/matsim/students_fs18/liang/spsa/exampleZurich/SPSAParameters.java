package ch.ethz.matsim.students_fs18.liang.spsa.exampleZurich;

public class SPSAParameters {
	public double perturbationPrefactor = 1.3;
	public double perturbationExponent = 0.101;

	public double gradientPrefactor = 0.7;
	public double gradientExponent = 0.602;
	
	public double maximumConstant = 10000.0;
	public double maximumUtilTravel = 0.0;
	public double minimumUtilMoney = 0.0;
	public double minimumPerforming = 0.0;
}
