package ch.ethz.matsim.students_fs18.liang.spsa;

import ch.ethz.matsim.students_fs18.liang.framework.simulation.SimulationHandle;
import ch.ethz.matsim.students_fs18.liang.spsa.candidate.CandidatesGenerator;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;

import java.io.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;

public class SPSAUpdate {

    private SPSAEnvironment spsaEnvironment;
    private CandidatesGenerator candidatesGenerator;
    final private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public SPSAUpdate(SPSAEnvironment spsaEnvironment, CandidatesGenerator candidatesGenerator){
        this.spsaEnvironment = spsaEnvironment;
        this.candidatesGenerator = candidatesGenerator;
    }

    private List<Double> buildPerturbation() {
        List<Double> perturbation = new LinkedList<>();

        for (int i = 0; i < spsaEnvironment.numberOfDimensions; i++) {
            perturbation.add(spsaEnvironment.sampler.sample());
        }

        return perturbation;
    }

    private List<List<Double>> buildGradientCandidates(double c, List<Double> perturbation,
                                                       List<Double> objectiveCandidate) {
        List<Double> projectionCandidate = spsaEnvironment.projection.projectGradientCandidate(c, perturbation, objectiveCandidate);

        List<Double> firstGradientCandidate = new LinkedList<>();
        List<Double> secondGradientCandidate = new LinkedList<>();

        for (int i = 0; i < spsaEnvironment.numberOfDimensions; i++) {
            firstGradientCandidate.add(projectionCandidate.get(i) + c * perturbation.get(i));
            secondGradientCandidate.add(projectionCandidate.get(i) - c * perturbation.get(i));
        }

        spsaEnvironment.logger.info("New gradient candidates:");
        spsaEnvironment.logger.info("  - First gradient candidate: " + firstGradientCandidate);
        spsaEnvironment.logger.info("  - Second gradient candidate: " + secondGradientCandidate);

        if (projectionCandidate.equals(objectiveCandidate)) {
            spsaEnvironment.logger.info("  - No projection has been performed.");
        } else {

            spsaEnvironment.logger.info("  - Projection has been performed: " + projectionCandidate);
        }

        return Arrays.asList(firstGradientCandidate, secondGradientCandidate);
    }

    private void startIteration(List<Double> candidate)
            throws JsonGenerationException, JsonMappingException, FileSystemException, IOException {
        int n = spsaEnvironment.state.iterations.size() + 1;
        double c = spsaEnvironment.sequence.getPerturbationFactor(n);

        List<Double> perturbation = buildPerturbation();
        List<List<Double>> gradientCandidates = buildGradientCandidates(c, perturbation, candidate);

        SPSAEvaluation objectiveEvaluation = new SPSAEvaluation();
        objectiveEvaluation.description = spsaEnvironment.descriptionFactory.create(candidate);
        objectiveEvaluation.simulationId = spsaEnvironment.buildSimulationId("objective_" + n);

        SPSAEvaluation firstGradientEvaluation = new SPSAEvaluation();
        firstGradientEvaluation.description = spsaEnvironment.descriptionFactory.create(gradientCandidates.get(0));
        firstGradientEvaluation.simulationId = spsaEnvironment.buildSimulationId("first_gradient_" + n);

        SPSAEvaluation secondGradientEvaluation = new SPSAEvaluation();
        secondGradientEvaluation.description = spsaEnvironment.descriptionFactory.create(gradientCandidates.get(1));
        secondGradientEvaluation.simulationId = spsaEnvironment.buildSimulationId("second_gradient_" + n);

        SPSAIteration iteration = new SPSAIteration();
        iteration.objectiveEvaluation = objectiveEvaluation;
        iteration.firstGradientEvaluation = firstGradientEvaluation;
        iteration.secondGradientEvaluation = secondGradientEvaluation;
        iteration.perturbation = perturbation;
        iteration.candidate = candidate;
        iteration.gradientCandidates = gradientCandidates;
        iteration.startTime = df.format(new Date());

        spsaEnvironment.state.iterations.add(iteration);
        spsaEnvironment.state.lastObjectiveIteration = -1;
        spsaEnvironment.state.lastFirstGradientIteration = -1;
        spsaEnvironment.state.lastSecondGradientIteration = -1;

        SimulationHandle objectiveHandle = spsaEnvironment.simulationEnvironment.setup(objectiveEvaluation.simulationId,
                objectiveEvaluation.description);
        SimulationHandle firstGradientHandle = spsaEnvironment.simulationEnvironment.setup(firstGradientEvaluation.simulationId,
                firstGradientEvaluation.description);
        SimulationHandle secondGradientHandle = spsaEnvironment.simulationEnvironment.setup(secondGradientEvaluation.simulationId,
                secondGradientEvaluation.description);

        objectiveHandle.start();
        firstGradientHandle.start();
        secondGradientHandle.start();

        spsaEnvironment.save();
    }

    private boolean stopIfFinished(SPSAEvaluation evaluation, SimulationHandle handle, int iteration, int lastIteration) {
        if (handle.isRunning()) {
            for (int intermediateIteration = 0; intermediateIteration <= spsaEnvironment.numberOfSimulationIterations;
                 intermediateIteration += spsaEnvironment.intermediateObjectiveInterval) {
                if (intermediateIteration < iteration
                        && intermediateIteration > evaluation.lastIntermediateObjectiveIteration) {
                    evaluation.intermediateObjectives
                            .add(spsaEnvironment.objective.getIntermediateObjective(handle, intermediateIteration));
                    evaluation.lastIntermediateObjectiveIteration = intermediateIteration;
                }
            }

            if (iteration > spsaEnvironment.numberOfSimulationIterations) {
                handle.stop();
                evaluation.objective = spsaEnvironment.objective.getObjective(handle);
                handle.clearIterations();
                return true;
            } else {
                return false;
            }
        } else {
            if (lastIteration <= spsaEnvironment.numberOfSimulationIterations) {
                throw new IllegalStateException("Simulation died before finishing");
            }

            return true;
        }
    }

    private boolean updateIterations(int objectiveIteration, int firstGradientIteration, int secondGradientIteration)
            throws JsonGenerationException, JsonMappingException, FileSystemException, IOException {
        if ((objectiveIteration > spsaEnvironment.state.lastObjectiveIteration)
                || (firstGradientIteration > spsaEnvironment.state.lastFirstGradientIteration)
                || (secondGradientIteration > spsaEnvironment.state.lastSecondGradientIteration)) {
            spsaEnvironment.state.lastObjectiveIteration = objectiveIteration;
            spsaEnvironment.state.lastFirstGradientIteration = firstGradientIteration;
            spsaEnvironment.state.lastSecondGradientIteration = secondGradientIteration;
            spsaEnvironment.save();

            return true;
        }

        return false;
    }


    public void update() throws JsonGenerationException, JsonMappingException, FileSystemException, IOException {
        if (spsaEnvironment.state.iterations.size() == 0) {
            startIteration(spsaEnvironment.initialCandidate);
        } else {
            SPSAIteration spsaIteration = spsaEnvironment.state.iterations.get(spsaEnvironment.state.iterations.size() - 1);

            SimulationHandle objectiveHandle = spsaEnvironment.simulationEnvironment
                    .recover(spsaIteration.objectiveEvaluation.simulationId);
            SimulationHandle firstGradientHandle = spsaEnvironment.simulationEnvironment
                    .recover(spsaIteration.firstGradientEvaluation.simulationId);
            SimulationHandle secondGradientHandle = spsaEnvironment.simulationEnvironment
                    .recover(spsaIteration.secondGradientEvaluation.simulationId);


            int objectiveIteration = objectiveHandle.getIteration();
            int firstGradientIteration = firstGradientHandle.getIteration();
            int secondGradientIteration = secondGradientHandle.getIteration();

            boolean iterationFinished = true;

            iterationFinished &= stopIfFinished(spsaIteration.objectiveEvaluation, objectiveHandle, objectiveIteration,
                    spsaEnvironment.state.lastObjectiveIteration);
            iterationFinished &= stopIfFinished(spsaIteration.firstGradientEvaluation, firstGradientHandle,
                    firstGradientIteration, spsaEnvironment.state.lastFirstGradientIteration);
            iterationFinished &= stopIfFinished(spsaIteration.secondGradientEvaluation, secondGradientHandle,
                    secondGradientIteration, spsaEnvironment.state.lastSecondGradientIteration);

            if (iterationFinished) {

                spsaIteration.endTime = df.format(new Date());
                spsaEnvironment.logger.info("Iteration " + (spsaEnvironment.state.iterations.size() + 1) + " has finished:");
                spsaEnvironment.logger.info("  - Candidate objective: " + spsaIteration.objectiveEvaluation.objective);
                spsaEnvironment.logger.info("  - First gradient objective: " + spsaIteration.firstGradientEvaluation.objective);
                spsaEnvironment.logger.info("  - Second gradient objective: " + spsaIteration.secondGradientEvaluation.objective);

                int n = spsaEnvironment.state.iterations.size() + 1;
                String objectiveFile = spsaEnvironment.objective.getZurichConfig().environmentPath + "/result/distanceDistribution" + n + ".csv";
                spsaEnvironment.objective.getCollectors().writeResult(objectiveFile);
                //spsaEnvironment.objective.getCollectors().writeResult("/nas/hliang/liang_42/zurich_1pm/result/distanceDistribution" + n + ".csv");

                candidatesGenerator.update(spsaEnvironment);
                startIteration(candidatesGenerator.generate(spsaIteration));

                //startIteration(buildNextCandidate(spsaIteration));
            } else if (updateIterations(objectiveIteration, firstGradientIteration, secondGradientIteration)) {
                spsaEnvironment.logger.info(String.format("Iteration %d running. [Objective: %d , 1st Gradient: %d , 2nd Gradient: %d]",
                        spsaEnvironment.state.iterations.size(), objectiveIteration, firstGradientIteration, secondGradientIteration));
            }
        }
    }
}
