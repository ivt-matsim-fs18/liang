package ch.ethz.matsim.students_fs18.liang.spsa.analysis;

import ch.ethz.matsim.students_fs18.liang.analysis.BinnedSampleCollector;
import ch.ethz.matsim.students_fs18.liang.analysis.distribution_distances.AbsoluteDistance;
import org.matsim.api.core.v01.Coord;
import org.matsim.core.utils.geometry.CoordUtils;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Collectors {

    private HashMap <String, BinnedSampleCollector> collectors;
    private double objective;
    private HashMap<String, List<Double>> reference;
    private HashMap<String, List<Double>> binLists;
    private int numberOfBins;

    public Collectors (int numberOfBins){
        collectors = new HashMap<>();
        objective = 0.0;
        this.numberOfBins = numberOfBins;
    }

    public Collectors (HashMap<String, List<Double>> reference){
        collectors = new HashMap<>();
        objective = 0.0;
        this.reference = reference;
        String mode = reference.keySet().toArray()[0].toString();
        this.numberOfBins = reference.get(mode).size();
    }

    public HashMap<String, BinnedSampleCollector> getCollectors(){
        return collectors;
    }

    public HashMap<String, List<Double>> getBinLists(){ return binLists;}

    public double getObjective(String ob){

        this.binLists = AbsoluteToModeshare();

        switch (ob) {
            case "sum" :
                this.objective = sumObjective(binLists);
                break;
            case "max" :
                this.objective = maxDifferenceObjective(binLists);
                break;
            case "normalized" :
                this.objective = normalizedObjective(binLists);
                break;
            default:
                this.objective = sumObjective(binLists);

        }

        return objective;
    }

    public HashMap<String, List<Double>> AbsoluteToModeshare(){

        for (String mode : collectors.keySet()) {

            collectors.get(mode).initializeBinCounts();

            for (double sample : collectors.get(mode).getSamples()) {
                collectors.get(mode).addSampleToBin(sample);

                for (String key : collectors.keySet()) {
                    if (! key.equals(mode)) {
                        collectors.get(key).addSampleToOtherBin(mode, sample);
                    }
                }
            }
        }


        HashMap<String, List<Double>> binLists = new HashMap<>();

        for (String mode : collectors.keySet()) {

            double [] sumList = new double [numberOfBins];

            binLists.put(mode, collectors.get(mode).buildAbsoluteFrequencies());

            for (int i = 0; i < binLists.get(mode).size(); i++){
                sumList[i] += binLists.get(mode).get(i);
            }

            for (String key : collectors.get(mode).otherBinCounts.keySet()) {

                for (int i = 0; i < binLists.get(mode).size(); i++){
                    sumList[i] += collectors.get(mode).otherBinCounts.get(key).get(i);
                }
            }

            for (int i = 0; i < sumList.length; i++){
                double frequency = binLists.get(mode).get(i);
                if (sumList[i] > 0){
                    binLists.get(mode).set(i, frequency / sumList[i]);
                }
                else
                    binLists.get(mode).set(i, 0.0);
            }


        }


        return this.binLists = binLists;
    }

    private double sumObjective(HashMap<String, List<Double>>binLists){

        double objective = 0.0;

        for (String mode : reference.keySet()) {
            objective += new AbsoluteDistance().compute(reference.get(mode), binLists.get(mode));
        }

        return objective;
    }

    private double maxDifferenceObjective(HashMap<String, List<Double>>binLists){
        double objective = 0.0;

        for (String key : reference.keySet()) {
            for (int i = 0; i < binLists.get(key).size(); i++){
                objective = Math.max(objective, Math.abs(binLists.get(key).get(i) - reference.get(key).get(i)));
            }
        }

        return objective;
    }

    private double normalizedObjective(HashMap<String, List<Double>>binLists){
        double objective = 0.0;

        for (String key : reference.keySet()) {
            for (int i = 0; i < binLists.get(key).size(); i++){
                if (reference.get(key).get(i) > 0){
                    objective += Math.abs(binLists.get(key).get(i) - reference.get(key).get(i)) / reference.get(key).get(i);
                }
            }
        }

        return objective;
    }

    public void readData(Coord center, Coord origin, Coord destination, double radius, String mode, double distance) {

        if (CoordUtils.calcEuclideanDistance(center, origin) < radius
                && CoordUtils.calcEuclideanDistance(center, destination) < radius){
            if (collectors.containsKey(mode)) {
                collectors.get(mode).addSample(distance);
            } else {
                collectors.put(mode, new BinnedSampleCollector());
                collectors.get(mode).addSample(distance);
            }
        }
    }

    public void writeResult(String filename){


        DecimalFormat df = new DecimalFormat("#.###");


        try {
            PrintWriter writer = new PrintWriter(filename);
            StringBuilder sb = new StringBuilder();


            for (String key : binLists.keySet()){

                List<Double> rightList = collectors.get(key).getRight();

                sb.append(" ,");

                for (double i : rightList){

                    sb.append(df.format(i));
                    sb.append(',');
                }

                sb.append("\n");

                sb.append(key);
                sb.append(',');
                for (double frequency : binLists.get(key)){
                    sb.append(df.format(frequency));
                    sb.append(',');
                }

                sb.append("\n");

            }

            writer.write(sb.toString());
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
