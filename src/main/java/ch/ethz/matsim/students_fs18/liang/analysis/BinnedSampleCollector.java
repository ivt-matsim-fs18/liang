package ch.ethz.matsim.students_fs18.liang.analysis;

import java.util.*;
import java.util.stream.Collectors;

public class BinnedSampleCollector {
    private List<Double> right = new ArrayList<>();
    private List<Double> binCounts = new LinkedList<>();
    public HashMap<String, List<Double>> otherBinCounts = new HashMap<>();
    private List<Double> samples = new ArrayList<>();

    public BinnedSampleCollector(){

    }

    public BinnedSampleCollector(List<Double> right) {
        this.right = right;

        initializeBinCounts();
    }

    public void addSampleToBin(double sample) {
        int index = 0;

        while (index < right.size() && right.get(index) < sample) {
            index++;
        }

        if (index <= right.size()) {
            binCounts.set(index, binCounts.get(index) + 1.0);
        }
    }

    public void addSampleToOtherBin(String mode, double sample) {

        if (! otherBinCounts.containsKey(mode)) {
            otherBinCounts.put(mode, new ArrayList<>());

            for (int i = 0; i <= right.size(); i++) {
                otherBinCounts.get(mode).add(0.0);
            }
        }

        int index = 0;

        while (index < right.size() && right.get(index) < sample) {
            index++;
        }

        if (index <= right.size()) {
            otherBinCounts.get(mode).set(index, otherBinCounts.get(mode).get(index) + 1.0);
        }

    }

    public void addSample(double sample) {
        samples.add(sample);
    }

    public List<Double> calculateBinSize (int numberOfBins) {
        Collections.sort(samples);
        right = new ArrayList<>();

        double binSize = samples.size() / numberOfBins;
        if ((int) binSize > 0) {
            for (int i = 1; i < numberOfBins; i ++) {
                right.add(samples.get((int)(binSize * i)));
            }
        }

        return right;

    }

    public void initializeBinCounts() {
        binCounts = new LinkedList<>();

        for (int i = 0; i <= right.size(); i++) {
            binCounts.add(0.0);
        }
    }

    public List<Double> buildAbsoluteFrequencies() {
        return binCounts;
    }

    public List<Double> buildRelativeFrequencies() {
        double total = binCounts.stream().mapToDouble(d -> d).sum();
        return binCounts.stream().map(d -> d / total).collect(Collectors.toList());
    }

    public void setRight(List<Double> right) {
        this.right = right;
    }

    public List<Double> getRight(){
        return right;
    }

    public List<Double> getSamples() {
        return samples;
    }
}
