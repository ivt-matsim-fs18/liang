package ch.ethz.matsim.students_fs18.liang.spsa;

import java.util.List;

public class SPSAIteration {
    public String startTime;
    public String endTime;
    public List<Double> candidate;
    public List<List<Double>> gradientCandidates;
    public List<Double> perturbation;

    public SPSAEvaluation firstGradientEvaluation;
    public SPSAEvaluation secondGradientEvaluation;
    public SPSAEvaluation objectiveEvaluation;

}
