package ch.ethz.matsim.students_fs18.liang.analysis.distribution_distances;

import java.util.List;

public interface DistributionDistance {
	double compute(List<Double> referenceBins, List<Double> simulationBins);
}
