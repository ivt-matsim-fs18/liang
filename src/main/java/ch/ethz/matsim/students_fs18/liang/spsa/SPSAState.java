package ch.ethz.matsim.students_fs18.liang.spsa;

import java.util.LinkedList;
import java.util.List;

public class SPSAState {
    public List<SPSAIteration> iterations = new LinkedList<>();

    public int lastObjectiveIteration;
    public int lastFirstGradientIteration;
    public int lastSecondGradientIteration;
}
