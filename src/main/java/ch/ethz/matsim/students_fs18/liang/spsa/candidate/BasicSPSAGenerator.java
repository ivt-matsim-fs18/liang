package ch.ethz.matsim.students_fs18.liang.spsa.candidate;

import ch.ethz.matsim.students_fs18.liang.spsa.SPSAEnvironment;
import ch.ethz.matsim.students_fs18.liang.spsa.SPSAIteration;
import ch.ethz.matsim.students_fs18.liang.spsa.SPSASequence;
import ch.ethz.matsim.students_fs18.liang.spsa.SPSAState;
import ch.ethz.matsim.students_fs18.liang.spsa.projection.SPSAProjection;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class BasicSPSAGenerator implements CandidatesGenerator{

    private SPSAState state;
    private SPSASequence sequence;
    private SPSAProjection projection;

    private int numberOfDimensions;

    private Logger logger;

    @Override
    public List<Double> generate(SPSAIteration iteration) throws IOException {
        int n = state.iterations.size();

        double a = sequence.getGradientFactor(n);
        double c = sequence.getPerturbationFactor(n);

        List<Double> gradient = new LinkedList<>();
        double nominator = iteration.firstGradientEvaluation.objective - iteration.secondGradientEvaluation.objective;

        for (int i = 0; i < numberOfDimensions; i++) {
            double denominator = 2.0 * c * iteration.perturbation.get(i);
            gradient.add(nominator / denominator);
        }

        List<Double> candidate = new LinkedList<>();

        for (int i = 0; i < numberOfDimensions; i++) {
            candidate.add(iteration.candidate.get(i) - a * gradient.get(i));
        }

        List<Double> projectionCandidate = projection.projectObjectiveCandidate(candidate);

        logger.info("New objective candidate:");
        logger.info("  - Gradient: " + gradient);
        logger.info("  - Candidate: " + projectionCandidate);

        if (projectionCandidate.equals(candidate)) {
            logger.info("  - No projection has been performed");
        } else {
            logger.info("  - Projection has been performed from " + candidate);
        }

        return projectionCandidate;
    }


    @Override
    public void update(SPSAEnvironment spsaEnvironment) {
        this.state = spsaEnvironment.state;
        this.sequence = spsaEnvironment.sequence;
        this.numberOfDimensions = spsaEnvironment.numberOfDimensions;
        this.projection = spsaEnvironment.projection;
        this.logger = spsaEnvironment.logger;
    }
}
