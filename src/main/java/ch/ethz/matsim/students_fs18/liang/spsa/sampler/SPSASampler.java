package ch.ethz.matsim.students_fs18.liang.spsa.sampler;

public interface SPSASampler {
	double sample();
}
