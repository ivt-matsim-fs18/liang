package ch.ethz.matsim.students_fs18.liang.analysis;

import javax.inject.Inject;
import org.matsim.core.controler.events.IterationEndsEvent;
import org.matsim.core.controler.events.ShutdownEvent;
import org.matsim.core.controler.listener.IterationEndsListener;
import org.matsim.core.controler.listener.ShutdownListener;
import org.matsim.core.scoring.ExperiencedPlansService;

class DistanceDistributionStatsControlerListener implements IterationEndsListener, ShutdownListener {

    @Inject
    private ExperiencedPlansService experiencedPlansService;
    @Inject
    private DistanceDistributionStats distanceDistributionStats;

    DistanceDistributionStatsControlerListener(){
    }

    public void notifyIterationEnds(IterationEndsEvent event) {
        this.distanceDistributionStats.calculate(event.getIteration(), this.experiencedPlansService.getExperiencedPlans());
    }

    public void notifyShutdown(ShutdownEvent event) {
        this.distanceDistributionStats.close();
    }
}



