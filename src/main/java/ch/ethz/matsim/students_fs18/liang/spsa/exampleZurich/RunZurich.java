package ch.ethz.matsim.students_fs18.liang.spsa.exampleZurich;

import ch.ethz.matsim.students_fs18.liang.framework.run.LocalLinuxRunEnvironment;
import ch.ethz.matsim.students_fs18.liang.framework.run.RunDescription;
import ch.ethz.matsim.students_fs18.liang.framework.simulation.SimulationConfigurator;
import ch.ethz.matsim.students_fs18.liang.framework.simulation.SimulationEnvironment;
import ch.ethz.matsim.students_fs18.liang.spsa.SPSAEnvironment;
import ch.ethz.matsim.students_fs18.liang.spsa.SPSASequence;
import ch.ethz.matsim.students_fs18.liang.spsa.SPSAUpdate;
import ch.ethz.matsim.students_fs18.liang.spsa.candidate.CandidatesGenerator;
import ch.ethz.matsim.students_fs18.liang.spsa.projection.SPSAProjection;
import ch.ethz.matsim.students_fs18.liang.spsa.sampler.RademacherSampler;
import ch.ethz.matsim.students_fs18.liang.spsa.sampler.SPSASampler;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Random;

public class RunZurich {

    static public void main(String[] args)
            throws JsonParseException, JsonMappingException, IOException, InterruptedException {
        ZurichConfig zurichConfig = new ObjectMapper().readValue(new File(args[0]), ZurichConfig.class);

        // Part 1: Set up the simulation environment
        File rootDirectory = new File(zurichConfig.environmentPath);
        LocalLinuxRunEnvironment runEnvironment = new LocalLinuxRunEnvironment(zurichConfig.environmentStateFile,
                rootDirectory);

        // Part 2: Set up the simulation templates

        SimulationConfigurator configurator = new ZurichSimulationConfigurator(zurichConfig);
        SimulationEnvironment simulationEnvironment = new SimulationEnvironment(new RunDescription(), runEnvironment,
                configurator);

        // Part 3: Set up SPSA

        ZurichDescriptionFactory descriptionFactory = new ZurichDescriptionFactory();
        ZurichObjective objective = new ZurichObjective(zurichConfig);
        SPSASampler sampler = new RademacherSampler(new Random());
        SPSAProjection projection = new ZurichProjection(zurichConfig.spsa);
        SPSASequence sequence = new ZurichSequence(zurichConfig.spsa);

        CandidatesGenerator candidatesGenerator = zurichConfig.ChooseMethod();

        SPSAEnvironment spsaEnvironment = new SPSAEnvironment(runEnvironment, simulationEnvironment, objective, sampler,
                projection, descriptionFactory, sequence, zurichConfig.spsaName,
                zurichConfig.initialCandidate, zurichConfig.numberOfIterations, zurichConfig.intermediateObjectiveInterval);

        SPSAUpdate spsa = new SPSAUpdate(spsaEnvironment,candidatesGenerator);

        while (true){
            spsa.update();
            Thread.sleep(1000);
        }
    }

}
