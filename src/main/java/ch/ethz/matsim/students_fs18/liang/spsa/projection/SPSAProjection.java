package ch.ethz.matsim.students_fs18.liang.spsa.projection;

import java.util.List;

public interface SPSAProjection {
	List<Double> projectGradientCandidate(double c, List<Double> perturbation, List<Double> candidate);
	List<Double> projectObjectiveCandidate(List<Double> candidate);
}
