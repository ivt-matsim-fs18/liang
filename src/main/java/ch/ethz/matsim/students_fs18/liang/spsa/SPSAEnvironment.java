package ch.ethz.matsim.students_fs18.liang.spsa;

import ch.ethz.matsim.students_fs18.liang.framework.run.RunEnvironment;
import ch.ethz.matsim.students_fs18.liang.framework.simulation.SimulationEnvironment;
import ch.ethz.matsim.students_fs18.liang.spsa.exampleZurich.ZurichObjective;
import ch.ethz.matsim.students_fs18.liang.spsa.projection.SPSAProjection;
import ch.ethz.matsim.students_fs18.liang.spsa.sampler.SPSASampler;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.List;

public class SPSAEnvironment {
	final public Logger logger;
	final public SimulationEnvironment simulationEnvironment;
	final public RunEnvironment runEnvironment;

	final public ZurichObjective objective;
	final public SPSASampler sampler;
	final public SPSAProjection projection;
	final public SPSADescriptionFactory descriptionFactory;
	final public SPSASequence sequence;

	final public SPSAState state;
	final private FileObject calibrationFile;
	final public String prefix;

	final public int numberOfDimensions;
	final public int numberOfSimulationIterations;
	final public int intermediateObjectiveInterval;
	final public List<Double> initialCandidate;
	
	final public ObjectMapper objectMapper;

	public SPSAEnvironment(RunEnvironment runEnvironment, SimulationEnvironment simulationEnvironment,
			ZurichObjective objective, SPSASampler sampler, SPSAProjection projection,
			SPSADescriptionFactory descriptionFactory, SPSASequence sequence, String prefix,
			List<Double> initialCandidate, int numberOfSimulationIterations, int intermediateObjectiveInterval)
			throws JsonParseException, JsonMappingException, IOException {
		this.objectMapper = new ObjectMapper();
		objectMapper.enable(SerializationFeature.INDENT_OUTPUT);

		this.runEnvironment = runEnvironment;
		this.simulationEnvironment = simulationEnvironment;
		this.calibrationFile = runEnvironment.getRootDirectory().resolveFile(prefix + "_spsa.json");
		this.prefix = prefix;

		this.objective = objective;
		this.sampler = sampler;
		this.projection = projection;
		this.descriptionFactory = descriptionFactory;
		this.sequence = sequence;

		this.numberOfDimensions = initialCandidate.size();
		this.initialCandidate = initialCandidate;
		this.numberOfSimulationIterations = numberOfSimulationIterations;
		this.intermediateObjectiveInterval = intermediateObjectiveInterval;

		this.logger = Logger.getLogger("SPSA " + prefix);

		if (calibrationFile.exists()) {
			state = objectMapper.readValue(calibrationFile.getContent().getInputStream(), SPSAState.class);
			logger.info("Initialized SPSA from " + calibrationFile.getName().getPath());
		} else {
			state = new SPSAState();
			logger.info("Initialized new SPSA at " + calibrationFile.getName().getPath());
		}
	}

	public String buildSimulationId(String id) {
		return prefix + "_" + id;
	}

	public void save() throws JsonGenerationException, JsonMappingException, FileSystemException, IOException {
		objectMapper.writeValue(calibrationFile.getContent().getOutputStream(), state);
	}

	/***
	private List<Double> buildPerturbation() {

		List<Double> perturbation = new LinkedList<>();

		for (int i = 0; i < numberOfDimensions; i++) {
			perturbation.add(sampler.sample());
		}

		return perturbation;
	}

	private List<List<Double>> buildGradientCandidates(double c, List<Double> perturbation,
			List<Double> objectiveCandidate) {
		List<Double> projectionCandidate = projection.projectGradientCandidate(c, perturbation, objectiveCandidate);

		List<Double> firstGradientCandidate = new LinkedList<>();
		List<Double> secondGradientCandidate = new LinkedList<>();

		for (int i = 0; i < numberOfDimensions; i++) {
			firstGradientCandidate.add(projectionCandidate.get(i) + c * perturbation.get(i));
			secondGradientCandidate.add(projectionCandidate.get(i) - c * perturbation.get(i));
		}

		logger.info("New gradient candidates:");
		logger.info("  - First gradient candidate: " + firstGradientCandidate);
		logger.info("  - Second gradient candidate: " + secondGradientCandidate);

		if (projectionCandidate.equals(objectiveCandidate)) {
			logger.info("  - No projection has been performed.");
		} else {

			logger.info("  - Projection has been performed: " + projectionCandidate);
		}

		return Arrays.asList(firstGradientCandidate, secondGradientCandidate);
	}

	private void startIteration(List<Double> candidate)
			throws JsonGenerationException, JsonMappingException, FileSystemException, IOException {
		int n = state.iterations.size() + 1;
		double c = sequence.getPerturbationFactor(n);

		List<Double> perturbation = buildPerturbation();
		List<List<Double>> gradientCandidates = buildGradientCandidates(c, perturbation, candidate);

		SPSAEvaluation objectiveEvaluation = new SPSAEvaluation();
		objectiveEvaluation.description = descriptionFactory.create(candidate);
		objectiveEvaluation.simulationId = buildSimulationId("objective_" + n);

        SPSAEvaluation firstGradientEvaluation = new SPSAEvaluation();
		firstGradientEvaluation.description = descriptionFactory.create(gradientCandidates.get(0));
		firstGradientEvaluation.simulationId = buildSimulationId("first_gradient_" + n);

        SPSAEvaluation secondGradientEvaluation = new SPSAEvaluation();
		secondGradientEvaluation.description = descriptionFactory.create(gradientCandidates.get(1));
		secondGradientEvaluation.simulationId = buildSimulationId("second_gradient_" + n);

		SPSAIteration iteration = new SPSAIteration();
		iteration.objectiveEvaluation = objectiveEvaluation;
		iteration.firstGradientEvaluation = firstGradientEvaluation;
		iteration.secondGradientEvaluation = secondGradientEvaluation;
		iteration.perturbation = perturbation;
		iteration.candidate = candidate;
		iteration.gradientCandidates = gradientCandidates;

		state.iterations.add(iteration);
		state.lastObjectiveIteration = -1;
		state.lastFirstGradientIteration = -1;
		state.lastSecondGradientIteration = -1;

		SimulationHandle objectiveHandle = simulationEnvironment.setup(objectiveEvaluation.simulationId,
				objectiveEvaluation.description);
		SimulationHandle firstGradientHandle = simulationEnvironment.setup(firstGradientEvaluation.simulationId,
				firstGradientEvaluation.description);
		SimulationHandle secondGradientHandle = simulationEnvironment.setup(secondGradientEvaluation.simulationId,
				secondGradientEvaluation.description);

		objectiveHandle.start();
		firstGradientHandle.start();
		secondGradientHandle.start();

		save();
	}

	private boolean stopIfFinished(SPSAEvaluation evaluation, SimulationHandle handle, int iteration, int lastIteration) {
		if (handle.isRunning()) {
			for (int intermediateIteration = 0; intermediateIteration <= numberOfSimulationIterations; intermediateIteration += intermediateObjectiveInterval) {
				if (intermediateIteration < iteration
						&& intermediateIteration > evaluation.lastIntermediateObjectiveIteration) {
					evaluation.intermediateObjectives
							.add(objective.getIntermediateObjective(handle, intermediateIteration));
					evaluation.lastIntermediateObjectiveIteration = intermediateIteration;
				}
			}

			if (iteration > numberOfSimulationIterations) {
				handle.stop();
				evaluation.objective = objective.getObjective(handle);
				int n = state.iterations.size() + 1;
				objective.getCollectors().writeResult("/nas/hliang/liang_30/zurich_1pm/result/distanceDistribution" + n + ".csv");
				handle.clearIterations();
				return true;
			} else {
				return false;
			}
		} else {
			if (lastIteration <= numberOfSimulationIterations) {
				throw new IllegalStateException("Simulation died before finishing");
			}

			return true;
		}
	}

	private boolean updateIterations(int objectiveIteration, int firstGradientIteration, int secondGradientIteration)
			throws JsonGenerationException, JsonMappingException, FileSystemException, IOException {
		if ((objectiveIteration > state.lastObjectiveIteration)
				|| (firstGradientIteration > state.lastFirstGradientIteration)
				|| (secondGradientIteration > state.lastSecondGradientIteration)) {
			state.lastObjectiveIteration = objectiveIteration;
			state.lastFirstGradientIteration = firstGradientIteration;
			state.lastSecondGradientIteration = secondGradientIteration;
			save();

			return true;
		}

		return false;
	}


	public void update() throws JsonGenerationException, JsonMappingException, FileSystemException, IOException {
		if (state.iterations.size() == 0) {
			startIteration(initialCandidate);
		} else {
			SPSAIteration spsaIteration = state.iterations.get(state.iterations.size() - 1);

			SimulationHandle objectiveHandle = simulationEnvironment
					.recover(spsaIteration.objectiveEvaluation.simulationId);
			SimulationHandle firstGradientHandle = simulationEnvironment
					.recover(spsaIteration.firstGradientEvaluation.simulationId);
			SimulationHandle secondGradientHandle = simulationEnvironment
					.recover(spsaIteration.secondGradientEvaluation.simulationId);


			int objectiveIteration = objectiveHandle.getIteration();
			int firstGradientIteration = firstGradientHandle.getIteration();
			int secondGradientIteration = secondGradientHandle.getIteration();

			boolean iterationFinished = true;

			iterationFinished &= stopIfFinished(spsaIteration.objectiveEvaluation, objectiveHandle, objectiveIteration,
					state.lastObjectiveIteration);
			iterationFinished &= stopIfFinished(spsaIteration.firstGradientEvaluation, firstGradientHandle,
					firstGradientIteration, state.lastFirstGradientIteration);
			iterationFinished &= stopIfFinished(spsaIteration.secondGradientEvaluation, secondGradientHandle,
					secondGradientIteration, state.lastSecondGradientIteration);

			if (iterationFinished) {
				logger.info("Iteration " + (state.iterations.size() + 1) + " has finished:");
				logger.info("  - Candidate objective: " + spsaIteration.objectiveEvaluation.objective);
				logger.info("  - First gradient objective: " + spsaIteration.firstGradientEvaluation.objective);
				logger.info("  - Second gradient objective: " + spsaIteration.secondGradientEvaluation.objective);

				candidatesGenerator.update(state, sequence, numberOfDimensions, projection, logger);
				startIteration(candidatesGenerator.generate(spsaIteration));

				//startIteration(buildNextCandidate(spsaIteration));
			} else if (updateIterations(objectiveIteration, firstGradientIteration, secondGradientIteration)) {
				logger.info(String.format("Iteration %d running. [Objective: %d , 1st Gradient: %d , 2nd Gradient: %d]",
						state.iterations.size(), objectiveIteration, firstGradientIteration, secondGradientIteration));
			}
		}
	}
	***/

	public SPSAState getState(){
		return this.state;
	}


}
