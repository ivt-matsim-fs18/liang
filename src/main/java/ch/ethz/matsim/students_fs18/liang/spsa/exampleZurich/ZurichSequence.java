package ch.ethz.matsim.students_fs18.liang.spsa.exampleZurich;

import ch.ethz.matsim.students_fs18.liang.spsa.SPSASequence;
import ch.ethz.matsim.students_fs18.liang.spsa.exampleZurich.SPSAParameters;

public class ZurichSequence implements SPSASequence {
    final private SPSAParameters parameters;

    public ZurichSequence(SPSAParameters parameters) {
        this.parameters = parameters;
    }

    @Override
    public double getGradientFactor(int n) {
        return parameters.gradientPrefactor / Math.pow(n, parameters.gradientExponent);
    }

    @Override
    public double getPerturbationFactor(int n) {
        return parameters.perturbationPrefactor / Math.pow(n, parameters.perturbationExponent);
    }
}
