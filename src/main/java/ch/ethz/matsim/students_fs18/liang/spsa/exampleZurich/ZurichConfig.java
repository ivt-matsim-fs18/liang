package ch.ethz.matsim.students_fs18.liang.spsa.exampleZurich;

import ch.ethz.matsim.students_fs18.liang.spsa.analysis.Collectors;

import ch.ethz.matsim.students_fs18.liang.spsa.candidate.BasicSPSAGenerator;
import ch.ethz.matsim.students_fs18.liang.spsa.candidate.CandidatesGenerator;
import ch.ethz.matsim.students_fs18.liang.spsa.candidate.GSMGenerator;
import ch.ethz.matsim.students_fs18.liang.spsa.candidate.PIGenerator;
import org.matsim.api.core.v01.Coord;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;
import java.text.DecimalFormat;

public class ZurichConfig {

    public String environmentStateFile = "zurich_state.json";
    //public String environmentPath = "/Users/lh/IdeaProjects/liang/zurich_1pm";
    public String environmentPath = "/nas/hliang/liang_46/zurich_1pm";
    public String spsaName = "zurich";

    public int numberOfIterations = 80;
    public int intermediateObjectiveInterval = 20;
    public List<Double> initialCandidate = Arrays.asList(-3.0, -3.0, -6.0, -4.0, 0.0, -6.0, 0.0, -10.0);


    public int numberOfGlobalThreads = 12;
    public int numberOfQSimThreads = 12;
    public String candidatesGeneratingMethod = "basic"; // "PI" , "GSM", "basic"
    public String objectiveFunction = "sum"; // "max", "normalized", "sum"

    public List<Double> center = Arrays.asList(2683253.0, 1246745.0);
    public double radius = 30000.0 - 1000.0;

    public SPSAParameters spsa = new SPSAParameters();

    public int numberOfBins = 5;
    public HashMap<String, List<Double>> boundsList = new HashMap<>();
    public List<Double> bounds = new ArrayList<>();

    public HashMap<String, List<Double>> reference = CalculateReference();

    static public void main(String args[]) throws JsonGenerationException, JsonMappingException, IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        objectMapper.writeValue(new File(args[0]), new ZurichConfig());

    }

    private HashMap<String, List<Double>> testReference (List<Double> bounds) {
        HashMap<String, List<Double>> reference = new HashMap<>();
        List<Double> carList = new ArrayList<>();
        List<Double> ptList = new ArrayList<>();
        for (int i = 0; i < bounds.size() + 1; i++){
            //carList.add(0.2 + 0.05 * i);
            //carList.add(0.5);
            //ptList.add(0.85 - carList.get(carList.size() - 1));
            ptList.add(0.35);
        }

        //reference.put("car", carList);
        reference.put("pt", ptList);

        List<Double> rightList = bounds;

        DecimalFormat df = new DecimalFormat("#.##");


        try {
            PrintWriter writer = new PrintWriter("reference_test.csv");
            StringBuilder sb = new StringBuilder();

            sb.append("mode share");
            sb.append(',');

            for (double r : rightList){
                sb.append(df.format(r));
                sb.append(',');
            }

            sb.append("\n");

            for (String key : reference.keySet()){
                sb.append(key);
                sb.append(',');

                for (double k : reference.get(key)){
                    sb.append(df.format(k));
                    sb.append(',');
                }

                sb.append("\n");
            }

            writer.write(sb.toString());
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return reference;
    }


    private HashMap<String, List<Double>> CalculateReference (){

        String referenceFile = environmentPath + "/trips_reference.csv";

        String line;
        String cvsSplitBy = ",";


        if (!bounds.isEmpty()) {
            numberOfBins = bounds.size() + 1;
        }

        Collectors collectors = new Collectors(numberOfBins);

        try {

            BufferedReader br = new BufferedReader(new FileReader(referenceFile));

            boolean isFirstRow = true;

            while ((line = br.readLine()) != null) {

                if (isFirstRow) {
                    isFirstRow = false;
                    continue;
                }

                String[] row = line.split(cvsSplitBy);
                String mode = row[2];

                Coord origin = new Coord(Double.parseDouble(row[3]), Double.parseDouble(row[4]));
                Coord destination = new Coord(Double.parseDouble(row[5]), Double.parseDouble(row[6]));
                Coord center = new Coord(this.center.get(0), this.center.get(1));

                StringBuilder modeSb = new StringBuilder(mode);
                modeSb.deleteCharAt(mode.length() - 1);
                modeSb.deleteCharAt(0);
                mode = modeSb.toString();
                double distance = Double.parseDouble(row[1]);

                collectors.readData(center, origin, destination, this.radius, mode, distance);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (String mode : collectors.getCollectors().keySet()) {
            if (! bounds.isEmpty()) {
                collectors.getCollectors().get(mode).setRight(bounds);
                boundsList.put(mode, bounds);
            } else {
                boundsList.put(mode, collectors.getCollectors().get(mode).calculateBinSize(numberOfBins));
            }
        }


        HashMap<String, List<Double>>  reference = collectors.AbsoluteToModeshare();

        collectors.writeResult("reference.csv");

        return reference;
    }



    public CandidatesGenerator ChooseMethod(){

        CandidatesGenerator generator;

        switch (candidatesGeneratingMethod) {
            case "PI" :
                generator = new PIGenerator();
                break;
            case "GSM" :
                generator = new GSMGenerator();
                break;
            default:
                generator = new BasicSPSAGenerator();

        }

        return generator;

    }
}




