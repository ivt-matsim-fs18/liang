package ch.ethz.matsim.students_fs18.liang.spsa;

import ch.ethz.matsim.students_fs18.liang.framework.simulation.SimulationDescription;

import java.util.List;

@FunctionalInterface
public interface SPSADescriptionFactory {
	SimulationDescription create(List<Double> candidate);
}
