package ch.ethz.matsim.students_fs18.liang.spsa.exampleZurich;

import ch.ethz.matsim.baseline_scenario.analysis.trips.TripItem;
import ch.ethz.matsim.baseline_scenario.analysis.trips.listeners.TripListener;
import ch.ethz.matsim.baseline_scenario.analysis.trips.utils.BaselineHomeActivityTypes;
import ch.ethz.matsim.baseline_scenario.analysis.trips.utils.HomeActivityTypes;
import ch.ethz.matsim.students_fs18.liang.analysis.BinnedSampleCollector;
import ch.ethz.matsim.students_fs18.liang.analysis.distribution_distances.EarthMoversDistance;
import ch.ethz.matsim.students_fs18.liang.framework.simulation.SimulationHandle;
import ch.ethz.matsim.students_fs18.liang.spsa.SPSAObjective;
import ch.ethz.matsim.students_fs18.liang.spsa.analysis.Collectors;
import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.api.experimental.events.EventsManager;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigReader;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.events.EventsUtils;
import org.matsim.core.events.MatsimEventsReader;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.io.MatsimNetworkReader;
import org.matsim.core.router.MainModeIdentifier;
import org.matsim.core.router.MainModeIdentifierImpl;
import org.matsim.core.router.StageActivityTypes;
import org.matsim.core.router.StageActivityTypesImpl;
import org.matsim.core.utils.geometry.CoordUtils;
import org.matsim.pt.PtConstants;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;

public class ZurichObjective implements SPSAObjective {
    final private ZurichConfig zurichConfig;
    final private Coord center;
    private double objective;
    private Collectors sampleCollectors;

    public ZurichObjective(ZurichConfig zurichConfig) {
        this.zurichConfig = zurichConfig;
        this.center = new Coord(zurichConfig.center.get(0), zurichConfig.center.get(1));
    }

    public Collectors getCollectors(){ return sampleCollectors; }

    @Override
    public double getObjective(SimulationHandle handle) {
        return getIntermediateObjective(handle, zurichConfig.numberOfIterations);
    }

    @Override
    public double getIntermediateObjective(SimulationHandle handle, int iteration) {
        try {
            Config config = ConfigUtils.createConfig();
            new ConfigReader(config)
                    .parse(handle.getDirectory().resolveFile("config.xml").getContent().getInputStream());

            Network network = NetworkUtils.createNetwork();
            new MatsimNetworkReader(network).parse(new GZIPInputStream(
                    handle.getDirectory().resolveFile(config.network().getInputFile()).getContent().getInputStream()));

            String eventsPath = "output/ITERS/it." + iteration + "/" + iteration + ".events.xml.gz";

            StageActivityTypes stageActivityTypes = new StageActivityTypesImpl(PtConstants.TRANSIT_ACTIVITY_TYPE);
            HomeActivityTypes homeActivityTypes = new BaselineHomeActivityTypes();
            MainModeIdentifier mainModeIdentifier = new MainModeIdentifierImpl();

            TripListener tripListener = new TripListener(network, stageActivityTypes, homeActivityTypes,
                    mainModeIdentifier);

            EventsManager eventsManager = EventsUtils.createEventsManager();
            eventsManager.addHandler(tripListener);
            new MatsimEventsReader(eventsManager).readStream(
                    new GZIPInputStream(handle.getDirectory().resolveFile(eventsPath).getContent().getInputStream()));
            Collection<TripItem> trips = tripListener.getTripItems();

            sampleCollectors = new Collectors(zurichConfig.reference);

            HashMap<String, BinnedSampleCollector> collectors = sampleCollectors.getCollectors();

            collectors.put("car", new BinnedSampleCollector(zurichConfig.boundsList.get("car")));
            collectors.put("pt", new BinnedSampleCollector(zurichConfig.boundsList.get("pt")));
            collectors.put("bike", new BinnedSampleCollector(zurichConfig.boundsList.get("bike")));
            collectors.put("walk", new BinnedSampleCollector(zurichConfig.boundsList.get("walk")));

            DecimalFormat df = new DecimalFormat("#.###");

            if (iteration == zurichConfig.numberOfIterations && handle.getId().startsWith("zurich_objective")){

                String filename = handle.getId().substring(17);

                try {

                    PrintWriter writer = new PrintWriter(zurichConfig.environmentPath + "/modeshare/modeshre_" + filename + ".csv");
                    StringBuilder sb = new StringBuilder();

                    sb.append(" ,network_distance,mode,origin_x,origin_y,destination_x,destination_y\n");

                    int index = 0;

                    for (TripItem item : trips) {


                        readData(item, collectors);

                        index += 1;

                        sb.append(index);
                        sb.append(",");

                        sb.append(df.format(item.networkDistance));
                        sb.append(",");
                        sb.append(item.mode);
                        sb.append(",");
                        sb.append(df.format(item.origin.getX()));
                        sb.append(",");
                        sb.append(df.format(item.origin.getY()));
                        sb.append(",");
                        sb.append(df.format(item.destination.getX()));
                        sb.append(",");
                        sb.append(df.format(item.destination.getY()));
                        sb.append("\n");


                    }

                    writer.write(sb.toString());
                    writer.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            else {
                for (TripItem item : trips) {
                    readData(item, collectors);
                }
            }



            objective = sampleCollectors.getObjective(zurichConfig.objectiveFunction);

            return objective;

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public ZurichConfig getZurichConfig() {
        return zurichConfig;
    }

    private void readData (TripItem item, HashMap<String, BinnedSampleCollector> collectors){

        if (CoordUtils.calcEuclideanDistance(center, item.origin) < zurichConfig.radius
                && CoordUtils.calcEuclideanDistance(center, item.destination) < zurichConfig.radius) {
            switch (item.mode) {
                case "car":
                    collectors.get("car").addSample(item.networkDistance);
                    break;
                case "pt":
                    collectors.get("pt").addSample(item.networkDistance);
                    break;
                case "bike":
                    collectors.get("bike").addSample(item.networkDistance);
                    break;
                case "walk":
                    collectors.get("walk").addSample(item.networkDistance);
                    break;
            }
        }
    }


}