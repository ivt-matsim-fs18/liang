package ch.ethz.matsim.students_fs18.liang.framework.run;

import org.apache.commons.vfs2.FileObject;

import java.util.Collection;

public interface RunEnvironment {
	void start(String id);

	void stop(String id);

	void setup(String id, RunDescription description);

	void remove(String id);

	boolean isRunning(String id);

	boolean exists(String id);

	FileObject getRootDirectory();

	FileObject getRunDirectory(String id);

	Collection<String> getAvailableIds();
}
