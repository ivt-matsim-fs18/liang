package ch.ethz.matsim.students_fs18.liang.spsa.candidate;

import ch.ethz.matsim.students_fs18.liang.framework.simulation.SimulationHandle;
import ch.ethz.matsim.students_fs18.liang.spsa.SPSAEnvironment;
import ch.ethz.matsim.students_fs18.liang.spsa.SPSAEvaluation;
import ch.ethz.matsim.students_fs18.liang.spsa.SPSAIteration;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.log4j.Logger;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;

public class GSMGenerator implements CandidatesGenerator {

    private SPSAEnvironment spsaEnvironment;
    private int numberOfDimensions;
    private Logger logger;
    private FileObject gsmFile;
    private GSMState state;
    private GSMIteration gsmIteration;

    final private double goldenSection = (Math.sqrt(5) - 1) / 2;
    private double tolerance = 0.1;

    private boolean isStart = false;
    private int GSMn = 0;

    private SPSAEvaluation firstEvaluation = new SPSAEvaluation();
    private SPSAEvaluation secondEvaluation = new SPSAEvaluation();
    private SimulationHandle firstHandle;
    private SimulationHandle secondHandle;

    final private ObjectMapper objectMapper = new ObjectMapper();;

    final private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    @Override
    public List<Double> generate(SPSAIteration iteration) throws IOException{

        this.objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        gsmFile = spsaEnvironment.runEnvironment.getRootDirectory().resolveFile(spsaEnvironment.prefix + "_gsm.json");

        if (gsmFile.exists()) {
            //state = objectMapper.readValue(gsmFile.getContent().getInputStream(), GSMState.class);
            logger.info("Initialized SPSA from " + gsmFile.getName().getPath());
        } else {
            state = new GSMState();
            logger.info("Initialized new SPSA at " + gsmFile.getName().getPath());
        }

        gsmIteration = new GSMIteration();

        List<Double> lowerBound = new ArrayList<>();
        List<Double> upperBound = new ArrayList<>();

        List<Double> x1 = new ArrayList<>();
        List<Double> x2 = new ArrayList<>();

        List<Double> newCandidate = new ArrayList<>();

        double distance = 0.0;
        GSMn = 0;

        int n = spsaEnvironment.getState().iterations.size();

        double a = spsaEnvironment.sequence.getGradientFactor(n);
        double c = spsaEnvironment.sequence.getPerturbationFactor(n);

        List<Double> gradient = new LinkedList<>();
        double nominator = iteration.firstGradientEvaluation.objective - iteration.secondGradientEvaluation.objective;

        for (int i = 0; i < numberOfDimensions; i++) {
            double denominator = 2.0 * c * iteration.perturbation.get(i);
            gradient.add(nominator / denominator);
        }


        for (int i = 0; i < numberOfDimensions; i++) {
            upperBound.add(iteration.candidate.get(i) - a * gradient.get(i));
            lowerBound.add(iteration.candidate.get(i));
        }

        for (int i = 0; i < numberOfDimensions; i++){
            x1.add(upperBound.get(i) - goldenSection * (upperBound.get(i) - lowerBound.get(i)));
            x2.add(lowerBound.get(i) + goldenSection * (upperBound.get(i) - lowerBound.get(i)));
            //distance += Math.abs(x1.get(i) - x2.get(i));
            distance = Math.max(distance, Math.abs(x1.get(i) - x2.get(i)));
        }



        tolerance = 0.01 + 0.1 * Math.pow(n, -0.5);

        gsmIteration.startTime = df.format(new Date());
        gsmIteration.tolerance = tolerance;
        gsmIteration.iteration = n;
        gsmIteration.numberOfGSMIteration = 0;
        gsmIteration.distance.add(distance);

        gsmIteration.lowerBound.add(copyList(lowerBound));
        gsmIteration.upperBound.add(copyList(upperBound));
        gsmIteration.x1.add(copyList(x1));
        gsmIteration.x2.add(copyList(x2));

        //while (distance / numberOfDimensions >= tolerance){
        while (distance >= tolerance){

            gsmIteration.numberOfGSMIteration += 1;


            simulationUpdate(x1, x2);
            distance = 0.0;

            gsmIteration.x1Object.add(firstEvaluation.objective);
            gsmIteration.x2Object.add(secondEvaluation.objective);

            if (firstEvaluation.objective < secondEvaluation.objective){
                for (int i = 0; i < numberOfDimensions; i++){

                    upperBound.set(i,x2.get(i));
                    x2.set(i, x1.get(i));
                    x1.set(i, upperBound.get(i) - goldenSection * (upperBound.get(i) - lowerBound.get(i)));
                    //distance += Math.abs(x1.get(i) - x2.get(i));
                    distance = Math.max(distance, Math.abs(x1.get(i) - x2.get(i)));
                }
            }
            else{

                for (int i = 0; i < numberOfDimensions; i++){

                    lowerBound.set(i,x1.get(i));
                    x1.set(i, x2.get(i));
                    x2.set(i, lowerBound.get(i) + goldenSection * (upperBound.get(i) - lowerBound.get(i)));
                    //distance += Math.abs(x1.get(i) - x2.get(i));
                    distance = Math.max(distance, Math.abs(x1.get(i) - x2.get(i)));
                }
            }

            gsmIteration.distance.add(distance);
            gsmIteration.lowerBound.add(copyList(lowerBound));
            gsmIteration.upperBound.add(copyList(upperBound));
            gsmIteration.x1.add(copyList(x1));
            gsmIteration.x2.add(copyList(x2));

            save();

        }


        for (int i = 0; i < numberOfDimensions; i++){

            newCandidate.add((lowerBound.get(i) + upperBound.get(i)) / 2);
        }

        gsmIteration.endTime = df.format(new Date());
        gsmIteration.newCandidate = newCandidate;

        save();

        return newCandidate;
    }


    @Override
    public void update(SPSAEnvironment spsaEnvironment) {
        this.spsaEnvironment = spsaEnvironment;
        this.numberOfDimensions = spsaEnvironment.numberOfDimensions;
        this.logger = spsaEnvironment.logger;
        this.objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
    }

    private void simulationStart(List<Double> x1, List<Double> x2) throws FileSystemException, IOException{

        GSMn += 1;

        int n = spsaEnvironment.getState().iterations.size();

        firstEvaluation = new SPSAEvaluation();
        firstEvaluation.description = spsaEnvironment.descriptionFactory.create(x1);
        firstEvaluation.simulationId = spsaEnvironment.buildSimulationId("firstGSM_" + n + "_" + GSMn);

        secondEvaluation = new SPSAEvaluation();
        secondEvaluation.description = spsaEnvironment.descriptionFactory.create(x2);
        secondEvaluation.simulationId = spsaEnvironment.buildSimulationId("secondGSM_" + n + "_" + GSMn);

        firstHandle = spsaEnvironment.simulationEnvironment.setup(firstEvaluation.simulationId,
                firstEvaluation.description);
        secondHandle = spsaEnvironment.simulationEnvironment.setup(secondEvaluation.simulationId,
                secondEvaluation.description);

        firstHandle.start();
        secondHandle.start();

        save();

    }

    private void simulationUpdate(List<Double> x1, List<Double> x2) throws FileSystemException, IOException{

        while (true) {
            if ( !isStart ){
                simulationStart(x1, x2);
                isStart = true;
            }

            else {
                boolean iterationFinished = true;

                iterationFinished &= stopIfFinished(firstEvaluation, firstHandle, firstHandle.getIteration());
                iterationFinished &= stopIfFinished(secondEvaluation, secondHandle, secondHandle.getIteration());

                if (!iterationFinished) {
                    logger.info(String.format("GSM Iteration %d running. [1st Gradient: %d , 2nd Gradient: %d]",
                            GSMn, firstHandle.getIteration(), secondHandle.getIteration()));
                } else {
                    logger.info("GSM Iteration " + GSMn + " has finished:");
                    logger.info("  - First gradient objective: " + firstEvaluation.objective);
                    logger.info("  - Second gradient objective: " + secondEvaluation.objective);

                    isStart = false;

                    break;
                }
            }

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(" Possibly still running.");
            }
        }

    }


    private boolean stopIfFinished(SPSAEvaluation evaluation, SimulationHandle handle, int iteration){
        if (handle.isRunning()) {
            if (iteration > spsaEnvironment.numberOfSimulationIterations) {
                handle.stop();
                evaluation.objective = spsaEnvironment.objective.getObjective(handle);
                handle.clearIterations();
                return true;
            } else {
                return false;
            }
        }
        else {
            if (iteration <= spsaEnvironment.numberOfSimulationIterations) {
                throw new IllegalStateException("Simulation died before finishing");
            }

            return true;
        }
    }

    private void save() throws JsonGenerationException, JsonMappingException, FileSystemException, IOException {

        if (state.iterations.size() < (spsaEnvironment.getState().iterations.size())){
            state.iterations.add(gsmIteration);
        }
        else {
            int i = spsaEnvironment.getState().iterations.size();
            state.iterations.set((i-1), gsmIteration);
        }

        objectMapper.writeValue(gsmFile.getContent().getOutputStream(), state);
    }

    private List<Double> copyList (List <Double> list) {
        List<Double> newList = new ArrayList<>();
        newList.addAll(list);

        return newList;
    }

    private class GSMState{

        public List<GSMIteration> iterations = new ArrayList<>();
    }

    private class GSMIteration{

        public int iteration;
        public int numberOfGSMIteration = 0;

        public List<List<Double>> upperBound = new ArrayList<>();
        public List<List<Double>> lowerBound = new ArrayList<>();
        public List<List<Double>> x1 = new ArrayList<>();
        public List<List<Double>> x2 = new ArrayList<>();

        public List<Double> x1Object = new ArrayList<>();
        public List<Double> x2Object = new ArrayList<>();

        public List<Double> newCandidate = new ArrayList<>();

        public double tolerance;
        public List<Double> distance = new ArrayList<>();

        public String startTime;
        public String endTime;

    }

}

