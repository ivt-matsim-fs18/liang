package ch.ethz.matsim.students_fs18.liang.analysis.distribution_distances;

import java.util.LinkedList;
import java.util.List;

public class AbsoluteDistance implements DistributionDistance {
    @Override
    public double compute(List<Double> referenceBins, List<Double> simulationBins) {
        List<Double> d = new LinkedList<>();
        double sum = 0.0;

        for (int i = 0; i < referenceBins.size(); i++) {
            d.add(simulationBins.get(i) - referenceBins.get(i));
            sum = Math.abs(d.get(i)) + sum;
        }

        return sum;
    }
}

