package ch.ethz.matsim.students_fs18.liang.spsa.projection;

import java.util.List;

public class UnconstrainedProjection implements SPSAProjection {
	@Override
	public List<Double> projectGradientCandidate(double c, List<Double> perturbation, List<Double> candidate) {
		return candidate;
	}

	@Override
	public List<Double> projectObjectiveCandidate(List<Double> candidate) {
		return candidate;
	}
}
