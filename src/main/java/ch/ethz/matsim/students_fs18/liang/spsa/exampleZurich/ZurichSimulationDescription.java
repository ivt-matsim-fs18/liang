package ch.ethz.matsim.students_fs18.liang.spsa.exampleZurich;

import ch.ethz.matsim.students_fs18.liang.framework.simulation.SimulationDescription;

public class ZurichSimulationDescription implements SimulationDescription {

    public double carConstant;
    public double carUtil;
    public double ptConstant;
    public double ptUtil;
    public double bikeConstant;
    public double bikeUtil;
    public double walkConstant;
    public double walkUtil;
    //public double utilOfMoney;
    //public double performingPara;



    //public double flowCapacityFactor;

    //public String chainId = null;
    //public String simulationId = null;

    public double objective;
}
