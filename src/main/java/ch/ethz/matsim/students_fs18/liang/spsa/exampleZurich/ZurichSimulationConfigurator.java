package ch.ethz.matsim.students_fs18.liang.spsa.exampleZurich;

import ch.ethz.matsim.students_fs18.liang.framework.run.RunDescription;
import ch.ethz.matsim.students_fs18.liang.framework.simulation.SimulationConfigurator;
import ch.ethz.matsim.students_fs18.liang.framework.simulation.SimulationDescription;
import ch.ethz.matsim.students_fs18.liang.framework.simulation.SimulationHandle;
import org.apache.commons.vfs2.AllFileSelector;
import org.apache.commons.vfs2.FileSystemException;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigReader;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.config.ConfigWriter;

import java.io.OutputStreamWriter;
import java.util.Arrays;

public class ZurichSimulationConfigurator implements SimulationConfigurator {

    final private ZurichConfig zurichConfig;

    public ZurichSimulationConfigurator(ZurichConfig zurichConfig) {
        this.zurichConfig = zurichConfig;
    }

    @Override
    public void configureSimulation(String id, SimulationDescription description, SimulationHandle handle) {
        try {

            Config config = ConfigUtils.createConfig(); //????
            new ConfigReader(config)
                    .parse(handle.getDirectory().resolveFile("../zurich_config.xml").getContent().getInputStream());

            //handle.getDirectory().resolveFile("stadtkreis").copyFrom(handle.getDirectory().resolveFile("../stadtkreis"),
                    //new AllFileSelector());

            config.facilities().setInputFile("../" + config.facilities().getInputFile());
            config.network().setInputFile("../" + config.network().getInputFile());
            config.plans().setInputFile("../" + config.plans().getInputFile());
            config.plans().setInputPersonAttributeFile("../" + config.plans().getInputPersonAttributeFile());
            config.households().setInputFile("../" + config.households().getInputFile());
            config.households()
                    .setInputHouseholdAttributesFile("../" + config.households().getInputHouseholdAttributesFile());
            config.transit().setTransitScheduleFile("../" + config.transit().getTransitScheduleFile());
            config.transit().setVehiclesFile("../" + config.transit().getVehiclesFile());
            config.controler().setOutputDirectory("output");

            //astraConfig.setScoringParametersPath("../" + astraConfig.getScoringParametersPath());
            //avConfig.setConfigPath("../" + avConfig.getConfigPath());

            ZurichSimulationDescription simulationDescription = (ZurichSimulationDescription) description;

            //astraConfig.setCrossingPenality(simulationDescription.crossingPenalty);
            //config.qsim().setFlowCapFactor(simulationDescription.flowCapacityFactor);

            config.planCalcScore().getModes().get("car").setConstant(simulationDescription.carConstant);
            config.planCalcScore().getModes().get("pt").setConstant(simulationDescription.ptConstant);
            config.planCalcScore().getModes().get("bike").setConstant(simulationDescription.bikeConstant);
            config.planCalcScore().getModes().get("walk").setConstant(simulationDescription.walkConstant);

            config.planCalcScore().getModes().get("car").setMarginalUtilityOfTraveling(simulationDescription.carUtil);
            config.planCalcScore().getModes().get("pt"). setMarginalUtilityOfTraveling(simulationDescription.ptUtil);
            config.planCalcScore().getModes().get("bike").setMarginalUtilityOfTraveling(simulationDescription.bikeUtil);
            config.planCalcScore().getModes().get("walk").setMarginalUtilityOfTraveling(simulationDescription.walkUtil);

            //config.planCalcScore().addParam("performing", Double.toString(simulationDescription.performingPara));
            //config.planCalcScore().addParam("marginalUtilityOfMoney", Double.toString(simulationDescription.utilOfMoney));


            config.controler().setWriteEventsInterval(zurichConfig.intermediateObjectiveInterval);

            new ConfigWriter(config).writeStream(new OutputStreamWriter(
                    handle.getDirectory().resolveFile("config.xml").getContent().getOutputStream()));
        } catch (FileSystemException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void configureRunner(String id, SimulationDescription description, RunDescription runDescription) {
        runDescription.classPath = Arrays.asList("/RunScenarion_jar/liang.jar");
        runDescription.entryPoint = "ch.ethz.matsim.students_fs18.liang.RunScenario";
        runDescription.vmArguments.add("");
        runDescription.arguments = Arrays.asList("config.xml", String.valueOf(zurichConfig.numberOfGlobalThreads),
                String.valueOf(zurichConfig.numberOfQSimThreads));
    }

}
