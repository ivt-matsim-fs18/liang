package ch.ethz.matsim.students_fs18.liang.framework.simulation;

import ch.ethz.matsim.students_fs18.liang.framework.run.RunDescription;

public interface SimulationConfigurator {
	void configureRunner(String id, SimulationDescription description, RunDescription runDescription);

	void configureSimulation(String id, SimulationDescription description, SimulationHandle handle);
}
