package ch.ethz.matsim.students_fs18.liang.spsa;

import ch.ethz.matsim.students_fs18.liang.framework.simulation.SimulationHandle;

public interface SPSAObjective {
	double getObjective(SimulationHandle handle);
	double getIntermediateObjective(SimulationHandle handle, int iteration);
}
