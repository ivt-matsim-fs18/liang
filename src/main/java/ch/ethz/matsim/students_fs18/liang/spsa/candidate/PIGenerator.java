package ch.ethz.matsim.students_fs18.liang.spsa.candidate;

import ch.ethz.matsim.students_fs18.liang.spsa.SPSAEnvironment;
import ch.ethz.matsim.students_fs18.liang.spsa.SPSAIteration;
import ch.ethz.matsim.students_fs18.liang.spsa.SPSASequence;
import ch.ethz.matsim.students_fs18.liang.spsa.SPSAState;
import ch.ethz.matsim.students_fs18.liang.spsa.projection.SPSAProjection;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class PIGenerator implements CandidatesGenerator{

    private SPSAState state;
    private SPSASequence sequence;
    private SPSAProjection projection;

    private int numberOfDimensions;

    private Logger logger;

    @Override
    public List<Double> generate(SPSAIteration iteration) {
       return null;
    }


    @Override
    public void update(SPSAEnvironment spsaEnvironment) {
        this.state = spsaEnvironment.state;
        this.sequence = spsaEnvironment.sequence;
        this.numberOfDimensions = spsaEnvironment.numberOfDimensions;
        this.projection = spsaEnvironment.projection;
        this.logger = spsaEnvironment.logger;
    }
}
